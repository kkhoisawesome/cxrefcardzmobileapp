using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CXDayMobileAppService.Startup))]

namespace CXDayMobileAppService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}