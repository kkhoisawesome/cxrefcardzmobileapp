using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CXDay.AzureMobileTest.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(FileController))]
namespace CXDay.AzureMobileTest.iOS
{
    public class FileController
    {
        public string GetPath(string name)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
            return Path.Combine(libraryPath, name);
        }
    }
}