﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CXDay.AzureMobileTest.Views.Cells
{
    public class ItemCell : ViewCell
    {
        readonly INavigation navigation;
        public ItemCell(INavigation navigation = null)
        {
            Height = 75;
            View = new ItemCellView();
            this.navigation = navigation;

        }
    }


    public partial class ItemCellView : ContentView
    {
        public ItemCellView()
        {
            InitializeComponent();
        }
    }
}
