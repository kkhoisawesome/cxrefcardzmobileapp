﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXDay.AzureMobileTest.ViewModels;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest.Views
{
    public partial class ListPage : ContentPage
    {
        ItemViewModel ViewModel => vm ?? (vm = BindingContext as ItemViewModel);
        ItemViewModel vm;
        public ListPage()
        {
            InitializeComponent();
            BindingContext = vm = new ItemViewModel();
            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone)
            {
                ToolbarItems.Add(new ToolbarItem
                {
                    Text = "Refresh",
                    Icon = "Resources/toolbar_refresh.png",
                    Command = vm.ForceRefreshCommand
                });
            }

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            UpdatePage();
        }

        void UpdatePage()
        {
            ViewModel?.LoadCardListCommand?.Execute(true);
        }
    }
}
