﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXDay.AzureMobileTest.Util;
using MvvmHelpers;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest
{
    public class ViewModelBase : BaseViewModel
    {
        protected INavigation Navigation { get; }

        public ViewModelBase(INavigation navigation = null)
        {
            Navigation = navigation;
        }

        public static void Init(bool mock = true)
        {
            DependencyService.Register<IStoreManager, StoreManager>();
            DependencyService.Register<ICardItemStore, CardItemStore>();
            Task.Run(async () =>
            {
                //the store must be initialized or else it will fail on first request
                await DependencyService.Get<IStoreManager>().InitializeAsync();
                await GetAllTablesFromAzureAsync();
            }).Wait(TimeSpan.FromSeconds(60));
        }

        protected IStoreManager StoreManager { get; } = DependencyService.Get<IStoreManager>();
        // protected IToast Toast { get; } = DependencyService.Get<IToast>();

        private static async Task GetAllTablesFromAzureAsync()
        {
            if (SettingsUtil.Current.FirstRun)
            {
                try
                {
                    var storeManager = DependencyService.Get<IStoreManager>();
                    await storeManager.SyncAllAsync(false);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            SettingsUtil.Current.FirstRun = false;
        }
    }
}
