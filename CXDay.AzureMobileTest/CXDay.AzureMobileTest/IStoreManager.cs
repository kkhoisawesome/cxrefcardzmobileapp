﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXDay.AzureMobileTest
{
    public interface IStoreManager
    {
        bool IsInitialized { get; }
        Task InitializeAsync();

        // TODO add stores
        ICardItemStore CardItemStore { get; }


        Task<bool> SyncAllAsync(bool syncUserSpecific);
        Task DropEverythingAsync();
    }
}
