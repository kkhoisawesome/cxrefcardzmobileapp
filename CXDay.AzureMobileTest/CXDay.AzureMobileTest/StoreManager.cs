﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXDay.AzureMobileTest.Models;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest
{
    public class StoreManager : IStoreManager
    {
        public static MobileServiceClient MobileService { get; set; }
        object locker = new object();
        public bool IsInitialized { get; private set; }
        public static string ApplicationURL = @"https://cxdaymobileapp.azurewebsites.net";


        public async Task InitializeAsync()
        {
            MobileServiceSQLiteStore store;
            lock (locker)
            {
                if (IsInitialized)
                    return;

                IsInitialized = true;
                SQLitePCL.Batteries.Init();
                var path = DependencyService.Get<IFileController>().GetPath(@"syncstore.db");
                MobileService = new MobileServiceClient(ApplicationURL);
                store = new MobileServiceSQLiteStore(path);
                store.DefineTable<StoreSettings>();
                // TODO DEFINE TABLE
                store.DefineTable<Cards>();
            }

            await MobileService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler()).ConfigureAwait(false);
        }

        private ICardItemStore cardItemStore;
        public ICardItemStore CardItemStore
            => cardItemStore ?? (cardItemStore = DependencyService.Get<ICardItemStore>());

        public async Task<bool> SyncAllAsync(bool syncUserSpecific)
        {
            if (!IsInitialized)
            {
                await InitializeAsync();
            }

            var taskList = new List<Task<bool>>();
            taskList.Add(CardItemStore.SyncAsync());


            var successes = await Task.WhenAll(taskList).ConfigureAwait(false);
            return successes.Any(x => !x); //if any were a failure.

        }

        public Task DropEverythingAsync()
        {
            CardItemStore.DropTable();
            IsInitialized = false;
            return Task.FromResult(true);
        }

        async Task SaveSettingsAsync(StoreSettings settings) =>
    await
        MobileService.SyncContext.Store.UpsertAsync(nameof(StoreSettings), new[] { JObject.FromObject(settings) },
            true);

        async Task<StoreSettings> ReadSettingsAsync() =>
            (await MobileService.SyncContext.Store.LookupAsync(nameof(StoreSettings), StoreSettings.StoreSettingsId))?
                .ToObject<StoreSettings>();

        public class StoreSettings
        {
            public const string StoreSettingsId = "store_settings";

            public StoreSettings()
            {
                Id = StoreSettingsId;
            }

            public string Id { get; set; }

            public string UserId { get; set; }
        }

    }
}
