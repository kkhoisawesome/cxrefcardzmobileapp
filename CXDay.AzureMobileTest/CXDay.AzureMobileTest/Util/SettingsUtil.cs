﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace CXDay.AzureMobileTest.Util
{
    public class SettingsUtil : INotifyPropertyChanged
    {
        #region Properties

        static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        static SettingsUtil settings;

        public static SettingsUtil Current
        {
            get { return settings ?? (settings = new SettingsUtil()); }
        }

        const string FirstRunKey = "first_run";
        static readonly bool FirstRunDefault = true;

        public bool FirstRun
        {
            get { return AppSettings.GetValueOrDefault<bool>(FirstRunKey, FirstRunDefault); }
            set
            {
                if (AppSettings.AddOrUpdateValue<bool>(FirstRunKey, value))
                    OnPropertyChanged();
            }
        }

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string name = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        #endregion
    }
}
