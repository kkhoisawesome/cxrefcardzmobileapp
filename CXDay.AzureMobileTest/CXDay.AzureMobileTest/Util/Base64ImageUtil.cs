﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest.Util
{
    public class Base64ImageUtil
    {
        public static ImageSource DecodeImage(string base64String)
        {
            ImageSource imageSource = null;
            if (base64String != null && !string.IsNullOrEmpty(base64String))
            {
                byte[] imageFotoBase64 = System.Convert.FromBase64String(base64String);
                imageSource = ImageSource.FromStream(() => new MemoryStream(imageFotoBase64));
            }

            return imageSource;
        }
    }
}
