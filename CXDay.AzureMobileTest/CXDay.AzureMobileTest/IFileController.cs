﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CXDay.AzureMobileTest
{
    public interface IFileController
    {
        string GetPath(string name);
    }
}
