﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CXDay.AzureMobileTest.Util;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest.Models
{
    public class Cards : BaseDataObject
    {
        ImageSource presenterPicture;

        public string Title { get; set; }
        public string Authors { get; set; }
        public string Image { get; set; }

        public string DescriptionInHtml { get; set; }

        [JsonIgnore]
        public ImageSource PresenterPicture
        {
            get
            {
                if (!IsPresenterPictureAvailable)
                {
                    var getBase64Code = Regex.Replace(Image, ".*,", String.Empty);
                    presenterPicture = Base64ImageUtil.DecodeImage(getBase64Code);
                }

                return presenterPicture;
            }
            set { SetProperty(ref presenterPicture, value); }
        }

        [JsonIgnore]
        public bool IsPresenterPictureAvailable
        {
            get
            {
                return presenterPicture != null;
            }
        }
    }
}
