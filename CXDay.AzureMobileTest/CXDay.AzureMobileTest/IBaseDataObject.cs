﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using Newtonsoft.Json;

namespace CXDay.AzureMobileTest
{
    public interface IBaseDataObject
    {
        string Id { get; set; }
    }

    public class BaseDataObject : ObservableObject, IBaseDataObject
    {
        public BaseDataObject()
        {
            Id = Guid.NewGuid().ToString();
        }

        [JsonIgnore]
        public string RemoteId { get; set; }

        public string Id { get; set; }
    }
}
