﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CXDay.AzureMobileTest.Models;
using MvvmHelpers;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest.ViewModels
{
    public class ItemViewModel : ViewModelBase
    {
        private bool noCardsFound;
        private string noCardsFoundMessage;
        private ICommand forceRefreshCommand;
        private ICommand loadCardListCommand;

        public ObservableRangeCollection<Cards> Cards { get; } = new ObservableRangeCollection<Cards>();

        #region Commands 

        public ICommand ForceRefreshCommand =>
            forceRefreshCommand ??
            (forceRefreshCommand = new Command(async () => await ExecuteForceRefreshCommandAsync(true)));

        async Task ExecuteForceRefreshCommandAsync(bool force = false)
        {
            await ExecuteLoadCardListAsync(force);
        }

        public ICommand LoadCardListCommand =>
            loadCardListCommand ??
            (loadCardListCommand = new Command(async () => await ExecuteLoadCardListAsync()));

        private async Task<bool> ExecuteLoadCardListAsync(bool force = false)
        {
            if (IsBusy)
            {
                return false;
            }

            try
            {
                IsBusy = true;
                NoCardsFound = false;


                Cards.ReplaceRange(await StoreManager.CardItemStore.GetItemsAsync(force));

                if (!Cards.Any())
                {
                    NoCardsFoundMessage = "No Cards found";
                    NoCardsFound = true;
                }

            }
            catch (Exception ex)
            {
                NoCardsFound = false;
            }

            IsBusy = false;

            return true;
        }

        #endregion

        public bool NoCardsFound
        {
            get
            {
                return noCardsFound;
            }
            set { SetProperty(ref noCardsFound, value); }
        }

        public string NoCardsFoundMessage
        {
            get { return noCardsFoundMessage; }
            set { SetProperty(ref noCardsFoundMessage, value); }
        }

    }
}
