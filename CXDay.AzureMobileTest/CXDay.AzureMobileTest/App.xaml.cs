﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CXDay.AzureMobileTest.Views;
using Xamarin.Forms;

namespace CXDay.AzureMobileTest
{
    public partial class App : Application
    {
        public static App current;
        private bool firstRun = true;
        public App()
        {
            InitializeComponent();
            ViewModelBase.Init();
            MainPage = new ListPage();
        }

        public new static App Current
        {
            get
            {
                return (App)Application.Current;
            }
        }

        protected override void OnStart()
        {
            OnResume();
        }

        protected override void OnResume()
        {
        }

        public void SecondOnResume()
        {
            OnResume();
        }

        protected override void OnSleep()
        {
        }
    }
}
